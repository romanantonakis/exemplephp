<?php

require('../Model/messageRepository.php'); // on récupère les requetes base de donnée

$allMessages = getMessages();

if(MessagesExist($allMessages)){
    require("../view/displayMessages.php");
}

$allMessages->closeCursor();

function MessagesExist($allMessages){ // function qui vérifie si il existe au moins Un message
    if($allMessages->rowCount() > 0 ){
        return true;
    }
    return false;

}