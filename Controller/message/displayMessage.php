<?php
$id_message = getMessageIdFromURI();

require('../Model/messageRepository.php'); // on récupère les requetes base de donnée

$oneMessage = getMessage($id_message);

if(messageExist($oneMessage)){ // On vérifie que le message existe
    require("../view/displayMessage.php");
}

function getMessageIdFromURI(){ // Récupérer l'id à la fin de l'URL
    $myUrl = $_SERVER['REQUEST_URI'];
    $myUrl = explode("/", $myUrl) ;
    $id_message = end($myUrl) ;

    return $id_message;
}

$oneMessage->closeCursor();

function messageExist($oneMessage){ // function qui vérifie si il existe au moins Un message
    if($oneMessage->rowCount() > 0 ){
        return true;
    }
    return false;

}
