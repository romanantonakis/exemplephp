<?php
$id_message = getMessageIdFromURI();

if(messageExist($id_message)){ // On vérifie que le messge existe
	require('../Model/messageRepository.php'); // on récupère les requetes base de donnée

    $deleteMessage = deleteMessage($id_message);

    $deleteMessage->closeCursor();

    require("../view/deleteMessage.php");
}

function getMessageIdFromURI(){ // Récupérer l'id à la fin de l'URL
    $myUrl = $_SERVER['REQUEST_URI'];
    $myUrl = explode("/", $myUrl) ;
    $id_message = end($myUrl) ;

    return $id_message;
}


function messageExist($id_message){ // function qui vérifie si l'id n'est pas nul, si il ne l'ai pas applique la function pour recher l'utilisateur
    if($id_message== 0 ){
        return false;
    }
    return getUser($id_message)->fetch();

}