<?php
$id_message = getMessageIdFromURI();

if(messageExist($id_message)){ // On vérifie que l'utilisateur existe
	require('../Model/messageRepository.php'); // on récupère les requetes base de donnée

	$date_creation = $_POST['date_creation'];
    $id_user = $_POST['id_user'];
    $body = $_POST['body'];

    $updateMessage = UpdateUser($id_message, $date_creation, $id_user, $body);

    $updateMessage->closeCursor();

    require("../view/updateMessage.php");
}

function getMessageIdFromURI(){ // Récupérer l'id à la fin de l'URL
    $myUrl = $_SERVER['REQUEST_URI'];
    $myUrl = explode("/", $myUrl) ;
    $id_message = end($myUrl) ;

    return $id_message;
}


function messageExist($id_message){ // function qui vérifie si l'id n'est pas nul, si il ne l'ai pas applique la function pour recher l'utilisateur
    if($id_message== 0 ){
        return false;
    }
    return getUser($id_message)->fetch();

}
