<?php
$id_user = getUserIdFromURI();

require('../Model/userRepository.php'); // on récupère les requetes base de donnée

$oneUser = getUser($id_user);

if(userExist($oneUser)){ // On vérifie que l'utilisateur existe

    require("../view/displayUser.php");

}

$oneUser->closeCursor();


function getUserIdFromURI(){ // Récupérer l'id à la fin de l'URL
    $myUrl = $_SERVER['REQUEST_URI'];
    $myUrl = explode("/", $myUrl) ;
    $id_user = end($myUrl) ;

    return $id_user;
}

function userExist($oneUser){ // function qui vérifie si il existe au moins Un utilisateur
    if($oneUser->rowCount() > 0 ){
        return true;
    }

    return false;

}
