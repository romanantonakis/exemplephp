<?php

require('../Model/userRepository.php'); // on récupère les requetes base de donnée

$allUsers = getUsers();

if(usersExist($allUsers)){
    require("../view/displayUsers.php");
}

$allUsers->closeCursor();

function usersExist($allUsers){ // function qui vérifie si il existe au moins Un utilisateur
    if($allUsers->rowCount() > 0 ){
        return true;
    }
    return false;

}