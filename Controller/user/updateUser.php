<?php
$id_user = getUserIdFromURI();

if(userExist($id_user)){ // On vérifie que l'utilisateur existe
	require('../Model/userRepository.php'); // on récupère les requetes base de donnée

	$email = $_POST['email'];
    $pseudo = $_POST['pseudo'];
    $password = $_POST['password'];

    $updateUser = UpdateUser($id_user, $email, $pseudo, $password);

    $updateUser->closeCursor();

    require("../view/updateUser.php");
}

function getUserIdFromURI(){ // Récupérer l'id à la fin de l'URL
    $myUrl = $_SERVER['REQUEST_URI'];
    $myUrl = explode("/", $myUrl) ;
    $id_user = end($myUrl) ;

    return $id_user;
}


function userExist($id_user){ // function qui vérifie si l'id n'est pas nul, si il ne l'ai pas applique la function pour recher l'utilisateur
    if($id_user== 0 ){
        return false;
    }
    return getUser($id_user)->fetch();

}
