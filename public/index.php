<?= 'Hello Worlds';

$uri = $_SERVER['REQUEST_URI'];


// USER
if($result = match($uri, "/users")){
    require("../Controller/user/displayUsers.php");
    die;
}

if($result = match($uri, "/user/:id")){
    require("../Controller/user/displayUser.php");
    die;
}

if($result = match($uri, "/user/create")){
    require("../Controller/user/createUser.php");
    die;
}

if($result = match($uri, "/user/update/:id")){
    require("../Controller/user/updateUser.php");
    die;
}

if($result = match($uri, "/user/delete/:id")){
    require("../Controller/user/deleteUser.php");
    die;
}
// MESSAGE

if($result = match($uri, "/messages")){
    require("../Controller/message/displayMessages.php");
    die;
}

if($result = match($uri, "/message/:id")){
    require("../Controller/message/displayMessage.php");
    die;
}

if($result = match($uri, "/message/create")){
    require("../Controller/message/createMessage.php");
    die;
}

if($result = match($uri, "/message/update/:id")){
    require("../Controller/message/updateMessage.php");
    die;
}

if($result = match($uri, "/message/delete/:id")){
    require("../Controller/message/deleteMessage.php");
    die;
}

function match($url, $route){
    $path = preg_replace('#:([\w]+)#', '([^/]+)', $route);
    $regex = "#^$path$#i";
    if(!preg_match($regex, $url, $matches)){
        return false;
    }
    return true;
}