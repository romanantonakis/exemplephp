<?php

function getUsers() {
	$bdd = bddConnect();

	$allUsers = $bdd->prepare('SELECT* FROM user');
	$allUsers->execute(array());

    return $allUsers;
}

function getUser($id_user) {
	$bdd =bddConnect();

	$oneUser = $bdd->prepare("SELECT* FROM user WHERE id='id_user'");

	$oneUser->execute(array('id_user'=>$id_user));
	return $oneUser;
}

function createUser($bddc, $email, $pseudo, $password) {

	$createUser = $bddc->prepare("INSERT INTO 'blog'.user('email', 'pseudo', 'password') VALUES(:email, :pseudo, :password)");
	$createUser = $bddc->execute(array(
									'email'=> $email,
									'pseudo'=> $pseudo,
									'password'=> $password));

	return $createUser;
}

function updateUser($id_user, $email, $pseudo, $password) {
	$bdd = bddConnect();

	$updateUser = $bdd->prepare("UPDATE user SET 'email'=:email, 'pseudo'=:pseudo, 'password'=:password WHERE id='id_user'");
	$updateUser = $bdd->execute(array(
									'id_user'=>$id_user,
									'email'=> $email,
									'pseudo'=> $pseudo,
									'password'=> $password));
	return $updateUser;
}

function deleteUser($id_user) {
	$bdd =bddConnect();

	$deleteUser = $bdd->prepare('DELETE* FROM user WHERE id="id_user"');
	$deleteUser = $bdd->execute(array(
									'id_user'=>$id_user));
	return $deleteUser;
}

function bddConnect() {
	try {
		return new PDO('mysql:host=localhost;dbname=blog;charset=utf8', 'root', '');
		
	} catch (Exception $e) {
		
	}
}